#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 13 20:50:52 2018

@author: Pritesh
"""
import random as rand #importing in Random Generator
import math as m      #importing in mathematical functions and constants 
import matplotlib.pyplot as plt # import mat plot lib 
import numpy as np

#Plotting convergence of Pi
def convergenceplot(yplot,counter,pieval):
    x = np.linspace(0,counter,counter//pieval)
    plt.plot(x,yplot,linewidth=1.5,color='blue')
    plt.ylabel('Error in $\pi$')
    plt.xlabel('Iterations, $N$'.format(counter//pieval))
    plt.axhline(0, color='black',linestyle='dashed')
    plt.show()
    return


#Computing Pi
def computepi(yplot,pieval,counter):
    inside = 0
    for i in range(1,counter+1):
        x = rand.uniform(-1,1)
        y = rand.uniform(-1,1)
        if(m.sqrt(x**2 +y**2) < 1): 
            inside +=1
        if(i%pieval ==0):
            yplot.append(float(4.0*inside/i)-m.pi)
    return yplot

#Parameters
counter = 1000 #Total Number of Iterations
pieval = 10  #Sampling Frequency of recording Pi value
yplot = []

#Calling Function to ComputePi
computepi(yplot,pieval,counter)

#Printing out values of Pi
print('Real pi {:0.8f}'.format( m.pi))
print("Approx pi = {0:.8f} Error in pi: {1:.8f} ".format(yplot[-1] + m.pi, yplot[-1]))

#Calling Functions to Plot Pi convergence
convergenceplot(yplot,counter,pieval)


##################################################################
#Random Notes/Equations for compuation.
##################################################################
#Area of circle 
r = 1 # radius 
Acircle = m.pi * r**2 #Since radius,r=1, Area = pi

#Since a square side lenght twice the radius of a circle
l = 2*r

#Area of square
Asquare = l**2 #Therefore the area of the square = 4

#if you pick N points at random inside the square, the ratio of them landing is Asquare:Acircle or
# 4:pi or more simply 1:(pi/4)
#approximately N*pi/4 of those points should fall inside the circle.




